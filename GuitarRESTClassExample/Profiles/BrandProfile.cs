﻿using AutoMapper;
using GuitarRESTClassExample.Models;
using GuitarRESTClassExample.Models.Dtos;

namespace GuitarRESTClassExample.Profiles
{
    public class BrandProfile: Profile
    {
        public BrandProfile()
        {
            CreateMap<CreateBrandDto, Brand>();
            CreateMap<Brand, BrandDto>()
                .ForMember(dto => dto.Guitars, options =>
                options.MapFrom(brandDomain => brandDomain.Guitars.Select(guitar => $"api/v1/guitars/{guitar.Id}").ToList()));
        }
    }
}
