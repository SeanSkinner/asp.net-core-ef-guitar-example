﻿namespace GuitarRESTClassExample.Exceptions
{
    public class GuitarNotFoundException: Exception
    {
        public GuitarNotFoundException(int id):base($"Guitar with id {id} was not found")
        {
        }
    }
}
