﻿using System.ComponentModel.DataAnnotations;

namespace GuitarRESTClassExample.Models
{
    public class Guitar
    {
        public int Id { get; set; }
        [MaxLength(20)]
        public string Description { get; set; }
        public Brand Brand { get; set; }
        public int BrandId { get; set; }
    }
}
