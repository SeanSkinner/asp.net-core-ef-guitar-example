﻿using GuitarRESTClassExample.Exceptions;
using GuitarRESTClassExample.Models;
using Microsoft.EntityFrameworkCore;

namespace GuitarRESTClassExample.Services
{
    public class GuitarService : IGuitarService
    {
        private readonly GuitarDbContext _context;

        public GuitarService(GuitarDbContext context)
        {
            _context = context;
        }

        public async Task<Guitar> AddGuitar(Guitar guitar)
        {
            _context.Guitars.Add(guitar);
            await _context.SaveChangesAsync();

            return guitar;
        }

        public async Task DeleteGuitar(int id)
        {
            var guitar = await _context.Guitars.FindAsync(id);
            if (guitar == null)
            {
                throw new GuitarNotFoundException(id);
            }

            _context.Guitars.Remove(guitar);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Guitar>> GetAllGuitars()
        {
            return await _context.Guitars.ToListAsync();
        }

        public async Task<Guitar> GetGuitarById(int id)
        {
            var guitar = await _context.Guitars.FindAsync(id);

            if (guitar == null)
            {
                throw new GuitarNotFoundException(id);
            }

            return guitar;
        }

        public async Task<Guitar> UpdateGuitar(Guitar guitar)
        {
            var foundGuitar = await _context.Guitars.AnyAsync(x => x.Id == guitar.Id);
            if (!foundGuitar)
            {
                throw new GuitarNotFoundException(guitar.Id);
            }
            _context.Entry(guitar).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return guitar;
        }
    }
}
