﻿using GuitarRESTClassExample.Models;

namespace GuitarRESTClassExample.Services
{
    public interface IGuitarService
    {
        Task<IEnumerable<Guitar>> GetAllGuitars();
        Task<Guitar> GetGuitarById(int id);
        Task<Guitar> AddGuitar(Guitar guitar);
        Task DeleteGuitar(int id);
        Task<Guitar> UpdateGuitar(Guitar guitar);
    }
}
