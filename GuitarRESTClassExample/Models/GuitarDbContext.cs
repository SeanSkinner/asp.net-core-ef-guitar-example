﻿using Microsoft.EntityFrameworkCore;

namespace GuitarRESTClassExample.Models
{
    public class GuitarDbContext: DbContext
    {
        public DbSet<Guitar> Guitars { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public GuitarDbContext(DbContextOptions options):base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brand>().HasData(
                new Brand { Id = 1, Name = "Fender"}
                );

            modelBuilder.Entity<Guitar>().HasData(
                new Guitar { Id = 1, Description = "Strat", BrandId = 1 },
                new Guitar { Id = 2, Description = "Telecaster", BrandId = 1 }
                );
        }
    }
}
