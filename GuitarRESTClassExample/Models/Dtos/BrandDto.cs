﻿namespace GuitarRESTClassExample.Models.Dtos
{
    public class BrandDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Guitars { get; set; }
    }
}
