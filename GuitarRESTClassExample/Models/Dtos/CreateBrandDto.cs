﻿namespace GuitarRESTClassExample.Models.Dtos
{
    public class CreateBrandDto
    {
        public string Name { get; set; }
    }
}
