﻿namespace GuitarRESTClassExample.Exceptions
{
    public class BrandNotFoundException: Exception
    {
        public BrandNotFoundException(int id):base($"Brand with id {id} was not found")
        {

        }
    }
}
