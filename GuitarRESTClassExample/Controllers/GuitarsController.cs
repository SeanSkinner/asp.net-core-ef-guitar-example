﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GuitarRESTClassExample.Models;
using GuitarRESTClassExample.Services;
using GuitarRESTClassExample.Exceptions;
using System.Net;
using System.Net.Mime;

namespace GuitarRESTClassExample.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiController]
    public class GuitarsController : ControllerBase
    {
        private readonly IGuitarService _guitarService;

        public GuitarsController(IGuitarService guitarService)
        {
            _guitarService = guitarService;
        }

        /// <summary>
        /// Gets all guitar resources
        /// </summary>
        /// <returns>List of Guitars</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Guitar>>> GetGuitars()
        {
            return Ok(await _guitarService.GetAllGuitars());
        }

        /// <summary>
        /// Gets a specific guitar based on a unique identifier
        /// </summary>
        /// <param name="id">A unique identifier for a guitar resource</param>
        /// <returns>A guitar resource</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Guitar>> GetGuitar(int id)
        {
            try
            {
                return await _guitarService.GetGuitarById(id);
            }
            catch (GuitarNotFoundException ex)
            {
                return NotFound(new ProblemDetails
                {
                    Detail = ex.Message
                });
            }
        }


        // POST: api/Guitars
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Guitar>> PostGuitar(Guitar guitar)
        {  
            return CreatedAtAction("GetGuitar", new { id = guitar.Id }, await _guitarService.AddGuitar(guitar));
        }

        // DELETE: api/Guitars/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGuitar(int id)
        {
            try
            {
                await _guitarService.DeleteGuitar(id);
            }
            catch (GuitarNotFoundException ex)
            {
                return NotFound(new ProblemDetails
                {
                    Detail = ex.Message
                });
            }

            return NoContent();
        }

        // PUT: api/Guitars/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGuitar(int id, Guitar guitar)
        {
            if (id != guitar.Id)
            {
                return BadRequest();
            }

            try
            {
                await _guitarService.UpdateGuitar(guitar);
            }
            catch (GuitarNotFoundException ex)
            {
                return NotFound(new ProblemDetails
                {
                    Detail = ex.Message
                });
            }

            return NoContent();
        }
    }
}
