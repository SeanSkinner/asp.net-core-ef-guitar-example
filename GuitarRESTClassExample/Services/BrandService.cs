﻿using GuitarRESTClassExample.Exceptions;
using GuitarRESTClassExample.Models;
using GuitarRESTClassExample.Models.Dtos;
using Microsoft.EntityFrameworkCore;

namespace GuitarRESTClassExample.Services
{
    public class BrandService : IBrandService
    {
        private readonly GuitarDbContext _context;

        public BrandService(GuitarDbContext context)
        {
            _context = context;
        }

        public async Task<Brand> CreateBrand(Brand brand)
        {
            await _context.Brands.AddAsync(brand);
            await _context.SaveChangesAsync();
            return brand;
        }

        public async Task<IEnumerable<Brand>> GetAllBrands()
        {
            return await _context.Brands.Include(x => x.Guitars).ToListAsync();
        }

        public async Task<Brand> GetBrandById(int id)
        {
            var brand = await _context.Brands.Include(x => x.Guitars).FirstOrDefaultAsync(x => x.Id == id);

            if (brand is null)
            {
                throw new BrandNotFoundException(id);
            }

            return brand;
        }
    }
}
