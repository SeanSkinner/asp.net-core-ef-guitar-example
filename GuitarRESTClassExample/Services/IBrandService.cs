﻿using GuitarRESTClassExample.Models;
using GuitarRESTClassExample.Models.Dtos;

namespace GuitarRESTClassExample.Services
{
    public interface IBrandService
    {
        Task<IEnumerable<Brand>> GetAllBrands();
        Task<Brand> CreateBrand(Brand brand);
        Task<Brand> GetBrandById(int id);
    }
}
