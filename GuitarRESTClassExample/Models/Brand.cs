﻿namespace GuitarRESTClassExample.Models
{
    public class Brand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Guitar> Guitars { get; set; }
    }
}
