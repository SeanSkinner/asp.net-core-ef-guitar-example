﻿using AutoMapper;
using GuitarRESTClassExample.Exceptions;
using GuitarRESTClassExample.Models;
using GuitarRESTClassExample.Models.Dtos;
using GuitarRESTClassExample.Services;
using Microsoft.AspNetCore.Mvc;

namespace GuitarRESTClassExample.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BrandsController: ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IMapper _mapper;

        public BrandsController(IBrandService brandService, IMapper mapper)
        {
            _brandService = brandService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BrandDto>>> GetAllBrands()
        {
            //manually mapping Dtos
            //var brands = await _brandService.GetAllBrands();
            //var brandDto = brands.Select(brand => new BrandDto
            //{
            //    Id = brand.Id,
            //    Name = brand.Name,
            //    Guitars = brand.Guitars.Select(g => g.Id.ToString()).ToList()
            //});
            //return Ok(brandDto);
            return Ok(_mapper.Map<IEnumerable<BrandDto>>(await _brandService.GetAllBrands()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BrandDto>> GetBrandById(int id)
        {
            try
            {
                return Ok(_mapper.Map<BrandDto>(await _brandService.GetBrandById(id)));
            }
            catch (BrandNotFoundException ex)
            {
                return NotFound(new ProblemDetails
                {
                    Detail = ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult<Brand>> CreateBrand(CreateBrandDto createBrandDto)
        {
            var brand = _mapper.Map<Brand>(createBrandDto);
            await _brandService.CreateBrand(brand);
            return CreatedAtAction(nameof(GetBrandById), new { id = brand.Id }, brand);
        }
    }
}
